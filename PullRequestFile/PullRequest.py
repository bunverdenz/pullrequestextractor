import requests
import pandas as pd
from os.path import join
import sys
import configparser
import datetime
import json
from datetime import timedelta

# # of times to request -> 3*(# of PRs)

DEFAULT_BASE_URL = "https://api.github.com"

HOME_DIR = '..'
CONFIG_FILE_PATH = '../../config/config.ini'


def load_app_key():
    config = configparser.ConfigParser()
    config.read(CONFIG_FILE_PATH)
    return config['github']['APP_KEY']


APP_KEY = load_app_key()
headers = {'Authorization': 'token %s' % APP_KEY}
now = datetime.datetime.now()
datetimeFormat = '%Y-%m-%dT%H:%M:%SZ'
strnow = now.strftime("%Y-%m-%dT%H:%M:%SZ")
payload_gen = {'state': 'all', 'per_page': '100'}
payload_commit = {'sha': 'master', 'per_page': '100'}
def _url(path):
    return DEFAULT_BASE_URL + path

def get_jobs(url,payload):
    # url = "https://api.angel.co/1/tags/1664/jobs"
    session = requests.Session()
    first_page = session.get(url, headers=headers, params=payload)
    # use link to find the number of page
    link = first_page.headers.get('link', None)
    if link is not None:
        index = link.find('rel="last"') - 3
        i = index
        while link[i] != '=':
            i -= 1
        num_pages = int(link[i+1:index])
    else:
        num_pages = 1
    yield first_page.json()
    # num_pages = first_page['last_page']
    payload_job = payload
    for page in range(2, num_pages + 1):
        payload_job['page'] = page
        next_page = session.get(url, headers=headers, params=payload_job)
        yield next_page.json()

def get_pull_request(owner, repo):
    # print(headers)
    # return requests.get(_url('/repos/%s/%s/pulls' % (owner, repo)), headers=headers, params=payload_gen)
    out = []
    for page in get_jobs(_url('/repos/%s/%s/pulls' % (owner, repo)), payload_gen):
        out = out + page
    return out

# already sorted in order of time -> the latest one is in the last index of the list
def get_comments_in_issue(owner, repo):
    # print(headers)
    # return requests.get(_url('/repos/%s/%s/issues/comments' % (owner, repo)), headers=headers)
    out = []
    for page in get_jobs(_url('/repos/%s/%s/issues/comments' % (owner, repo)), {'per_page': '100'}):
        out = out + page
    return out

# def get_con_rate(owner, repo):
#     print(headers)
#     return requests.get(_url('/repos/%s/%s/contributors' % (owner, repo)), headers=headers)

def get_commit(owner, repo):
    # print(headers)
    # return requests.get(_url('/repos/%s/%s/commits' % (owner, repo)), headers=headers, params=payload_commit)
    out = []
    for page in get_jobs(_url('/repos/%s/%s/commits' % (owner, repo)), payload_commit):
        out = out + page
    return out

class PullRequest(object):

    def __init__(self,json_pulls,json_commit,json_comments_in_issue,json_file,json_rev_comment):
        self.json_pulls = json_pulls
        self.json_commit = json_commit
        self.json_comments_in_issue = json_comments_in_issue
        self.json_files = json_file
        self.json_rev_comment = json_rev_comment
        self.age = None
        self.con_rate = None
        self.acc_rate = None
        self.line_add = None
        self.line_delete = None
        self.commit = None
        self.file = None
        self.comment = None
        self.review_com = None
        self.core_mem = None
        self.intra_branch = None
        self.issue_fix = None
        self.last_comment_mention = None
        self.has_test_code = None
        self.rev_change = None
        self.churn_rev = None
        self.self_approve = None
        self.hastily_rev = None
        self.no_discussion = None
        self.typ_discuss_len = None
        self.typ_rev_window = None
        self.involve_expert = None

        self.expert = {}
    def get_age(self):
        # get age between created time and the closed time of each pull request
        out = {}
        for item in self.json_pulls:
            date = item['created_at']
            if item['closed_at'] is None:
                diff = "not closed"
            else:
                strnow = item['closed_at']
                diff = datetime.datetime.strptime(strnow, datetimeFormat) \
                       - datetime.datetime.strptime(date, datetimeFormat)
                hours = (24 * diff.days)+((diff.seconds) / (60 * 60))
                diff = hours
            out[item['number']] = diff
        self.age = out
        return out
    def get_con_rate(self):
        # for each committer, get the percentage of their commit
        out = {}
        for item in self.json_pulls:
            user = item['user']['login']
            created_at = item['created_at']
            date_pr_create = datetime.datetime.strptime(created_at,datetimeFormat)
            out[item['number']] = 0
            # total commit before that PR is created
            total_commit_related_date = 0
            total_commit = 0
            all_user = {}
            for commit in self.json_commit:
                # get date of each commit
                if commit['commit'] is None or commit['commit']['author'] is None or commit['author'] is None:
                    continue
                date_commit_create = datetime.datetime.strptime(commit['commit']['author']['date'],datetimeFormat)
                if date_pr_create > date_commit_create:
                    total_commit_related_date += 1
                    if user == commit['author']['login']:
                        out[item['number']] += 1
                # Find subject expert using percentage of commit for all files
                if commit['author']['login'] not in all_user.keys():
                    all_user[commit['author']['login']] = 1
                else:
                    all_user[commit['author']['login']] += 1
                total_commit += 1
            for login in all_user.keys():
                percent = all_user[login]/total_commit
                # if percentage is more than 5%, he is subject expert
                if percent > 0.05:
                    self.expert[login] = percent
            if total_commit_related_date != 0:
                out[item['number']] = out[item['number']]/total_commit_related_date
            else:
                out[item['number']] = 'no commit before this PR'
        self.con_rate = out
        return out
    def get_acc_rate(self):
        # percentage of author's PR that have been merged
        out = {}
        for item in self.json_pulls:
            user = item['user']['login']
            created_at = item['created_at']
            date_pr_create = datetime.datetime.strptime(created_at, datetimeFormat)
            out[item['number']] = 0
            # total PR that the author create before this PR
            total_pr = 0
            for pull_before in self.json_pulls:
                date_before_create = datetime.datetime.strptime(pull_before['created_at'],datetimeFormat)
                # if the author of PR is the same as this PR and date created is before
                if date_pr_create > date_before_create and user == pull_before['user']['login']:
                    if user is not None:
                        if pull_before['merged_at'] is not None:
                            out[item['number']] += 1
                        total_pr += 1
            if total_pr != 0:
                out[item['number']] = out[item['number']]/total_pr
        self.acc_rate = out
        return out

    # For these metrics, each pull request has already provided the number of them

    def get_line_add(self):
        out = {}
        for item in self.json_pulls:
            out[item['number']] = item['additions']
        self.line_add = out
        return out
    def get_line_del(self):
        out = {}
        for item in self.json_pulls:
            out[item['number']] = item['deletions']
        self.line_delete = out
        return out
    def get_commit(self):
        out = {}
        for item in self.json_pulls:
            out[item['number']] = item['commits']
        self.commit = out
        return out
    def get_file(self):
        out = {}
        for item in self.json_pulls:
            out[item['number']] = item['changed_files']
        self.file = out
        return out
    def get_comment(self):
        # TODO: no automatic
        out = {}
        keep = {}
        auto = {}
        for item in self.json_comments_in_issue:
            # get the number of issue from issue_url
            # ex: issue_url: "https://api.github.com/repos/PyGithub/PyGithub/issues/20"
            num_issue = int(item['issue_url'].split("/")[-1])
            # if that issue is pull request
            if num_issue in pulls_num:
                keep[num_issue] = item['user']
        for num in keep.keys():
            # check auto comment by type = 'Bot'
            if keep[num]['type'] == 'Bot':
                if num not in auto.keys():
                    auto[num] = 1
                else:
                    auto[num] += 1
        for item in self.json_pulls:
            if item['number'] not in auto.keys():
                out[item['number']] = int(item['comments'])
            else:
                out[item['number']] = int(item['comments']) - auto[item['number']]
        self.comment = out
        return out
    def get_rev_comment(self):
        # TODO: no automatic, comments on code
        out = {}
        index = 0

        for item in self.json_pulls:
            auto = 0
            for rev_comment in self.json_rev_comment[index]:
                if rev_comment['user'] is not None:
                    if rev_comment['user']['type'] == 'Bot':
                        auto += 1
            index += 1
            out[item['number']] = int(item['review_comments']) - auto
        self.review_com = out
        return out

    # get core member which is "Collaborator" of the repository

    def get_core_mem(self):
        keep = {}
        out = {}
        # Get core member using pushing commit to master
        for item in self.json_commit:
            if item['committer'] is not None:
                if item['committer']['login'] is not None:
                    keep[item['committer']['login']] = True
        # Get core member using author_association == "COLLABORATOR"
        for item in self.json_pulls:
            if item['author_association'] == "COLLABORATOR":
                keep[item['user']['login']] = True

        # get the number of core member and not core member for all pulls
        for item in self.json_pulls:
            if item['user']['login'] in keep.keys():
                out[item['number']] = 'True'
            else:
                out[item['number']] = 'False'
        self.core_mem = out
        return out

    # check whether the branch that is used to merge, and the target branch is the same or not
    def get_intra_branch(self):
        out = {}
        for item in self.json_pulls:
            if item['head'] is not None and item['base'] is not None:
                if item['head']['repo'] is not None and item['base']['repo'] is not None:
                    if item['head']['repo']['id'] == item['base']['repo']['id']:
                        out[item['number']] = 'True'
                    else:
                        out[item['number']] = 'False'
                else:
                    out[item['number']] = 'False'
            else:
                out[item['number']] = 'False'
        self.intra_branch = out
        return out

    def get_issue_fix(self):
        # check the string "fix" whether the string is a substring of "title" or "body" of
        # each PR
        out = {}
        for item in self.json_pulls:
            if item['title'] is not None and "fix" in item['title']:
                out[item['number']] = 'True'
            elif item['body'] is not None and "fix" in item['body']:
                out[item['number']] = 'True'
            else:
                out[item['number']] = 'False'
        self.issue_fix = out
        return out
    def get_last_comment_mention(self, pulls_num):
        # User mention is used by using "@" , so I will check whether this letter appear
        # in the last comment i get from the issue of that pull requests (all PRs is issues)
        # not from review_comment of that PR
        out = {}
        # keep for keeping last comment for each issue
        keep = {}
        for item in self.json_comments_in_issue:
            # get the number of issue from issue_url
            # ex: issue_url: "https://api.github.com/repos/PyGithub/PyGithub/issues/20"
            num_issue = int(item['issue_url'].split("/")[-1])
            # if that issue is pull request
            if num_issue in pulls_num:
                keep[num_issue] = item['body']
        for num in keep.keys():
            if '@' in keep[num]:
                out[num] = 'True'
        for item in json_pulls:
            if item['number'] not in out.keys():
                out[item['number']] = 'False'
        self.last_comment_mention = out
        return out
    def get_has_test_code(self, pulls_num):
        # if “test” or “spec” are in
        # the file path of any file affected by the pull request, then the
        # pull request is marked as having tests
        out = {}
        index = 0
        for pull in self.json_files:
            check = 0
            for file in pull:
                # print(file)
                if "test" in file['filename'] or "spec" in file['filename']:
                    out[pulls_num[index]] = 'False'
                    check = 1
                    break
            if check == 0:
                out[pulls_num[index]] = 'False'
            index += 1
        self.has_test_code = out
        return out
    def get_rev_change(self):
        # Count the number of review_comment, if it's 0, no review change
        out = {}
        for item in self.json_pulls:
            if item['review_comments'] == 0:
                out[item['number']] = "False"
            else:
                out[item['number']] = "True"
        self.rev_change = out
        return out
    def get_churn_rev(self):
        out = {}
        for item in self.json_pulls:
            out[item['number']] = int(item['additions'])+int(item['deletions'])
        self.churn_rev = out
        return out
    def get_self_approve(self):
        # check if requested_reviewer is None or not
        out = {}
        for item in self.json_pulls:
            if item['requested_reviewers'] is not None:
                out[item['number']] = "True"
            else:
                out[item['number']] = "False"
        self.self_approve = out
        return out
    def get_hastily_rev(self,age_pulls):
        out = {}
        for item in self.json_pulls:
            addition = int(item['additions'])
            deletion = int(item['deletions'])
            if age_pulls[item['number']] == 'not closed':
                out[item['number']] = "False"
                continue
            age = float(age_pulls[item['number']])
            keep = (addition+deletion)/age
            if keep>200:
                out[item['number']] = "True"
            else:
                out[item['number']] = "False"
        self.review_com = out
        return out
    def get_no_discussion(self, pulls_num):
        # True if no discussion, false if there is non-automatic-discussion
        out = {}
        keep = {}
        for item in self.json_comments_in_issue:
            # get the number of issue from issue_url
            # ex: issue_url: "https://api.github.com/repos/PyGithub/PyGithub/issues/20"
            num_issue = int(item['issue_url'].split("/")[-1])
            # if that issue is pull request
            if num_issue in pulls_num:
                keep[num_issue] = item['user']
        for num in keep.keys():
            # check auto comment by type = 'Bot'
            if keep[num]['type'] != 'Bot':
                out[num] = 'False'
        for item in self.json_pulls:
            if item['number'] not in out.keys():
                out[item['number']] = 'True'
        self.no_discussion = out
        return out
    def get_typ_rev_window(self,age_pulls):
        out = {}
        for item in self.json_pulls:
            addition = int(item['additions'])
            deletion = int(item['deletions'])
            if age_pulls[item['number']] == 'not closed':
                out[item['number']] = "not closed"
                continue
            age = float(age_pulls[item['number']])
            if addition+deletion == 0:
                out[item['number']] = 'no additions and deletions'
                continue
            out[item['number']] = age/(addition + deletion)
        self.typ_rev_window = out
        return out
    def get_typ_discuss_len(self,comment_pulls):
        out = {}
        for item in self.json_pulls:
            addition = int(item['additions'])
            deletion = int(item['deletions'])
            comments = int(comment_pulls[item['number']])
            if addition+deletion == 0:
                out[item['number']] = 'no additions and deletions'
                continue
            out[item['number']] = comments / (addition + deletion)
        self.typ_discuss_len = out
        return out
    def get_involve_expert(self):
        out = {}
        index = 0
        for item in self.json_pulls:
            # check the review that is created by subject expert or not
            if item['requested_reviewers'] is not None:
                for reviewer in item['requested_reviewers']:
                    if reviewer['login'] in self.expert.keys():
                        out[item['number']] = "True"
                        continue
            # check if subject expert comment on review
            for comment in self.json_rev_comment[index]:
                if comment['user'] is not None:
                    if comment['user']['login'] in self.expert.keys():
                        out[item['number']] = "True"
                        continue
            if item['number'] not in out.keys():
                out[item['number']] = "False"
            index += 1
        self.involve_expert = out
        return out

if __name__ == '__main__':

    pulls_num = []
    json_pulls = []
    json_commit = []
    json_files = []
    json_rev_comment = []
    enter = input("enter github url\n")
    first = input("Is this the first time for this url?:y/n\n")
    github_url = enter.split("/")
    owner = github_url[3]
    repo_name = github_url[4]
    if first == 'y' or first == 'Y':

        # GET / repos /: owner /:repo / pulls /: number
        # First create a Github instance:

        resp_pull = get_pull_request(owner, repo_name)
        print("length")
        print(len(resp_pull))
        # resp_con_rate = get_con_rate(owner, repo_name)
        json_commit = get_commit(owner,repo_name)
        print("length")
        print(len(json_commit))
        json_comments_in_issue = get_comments_in_issue(owner,repo_name)
        print("length")
        print(len(json_comments_in_issue))
        # Request data of each pull request in json format
        for item in resp_pull:
            pulls_num.append(item['number'])
            json_pulls.append(requests.get(_url('/repos/%s/%s/pulls/%s' % (owner, repo_name, item['number'])), headers=headers).json())
            json_files.append(
                requests.get(_url('/repos/%s/%s/pulls/%s/files' % (owner, repo_name, item['number'])), headers=headers).json())
            json_rev_comment.append(
                requests.get(_url('/repos/%s/%s/pulls/%s/comments' % (owner, repo_name, item['number'])), headers=headers).json())
        print("Finish requesting data")
        print("save json file")

        with open('%s_%s_pulls_num.json' % (owner,repo_name), 'w') as outfile:
            json.dump(pulls_num, outfile)
        with open('%s_%s_json_pulls.json' % (owner,repo_name), 'w') as outfile:
            json.dump(json_pulls, outfile)
        with open('%s_%s_json_commit.json' % (owner,repo_name), 'w') as outfile:
            json.dump(json_commit, outfile)
        with open('%s_%s_json_comments_in_issue.json' % (owner,repo_name), 'w') as outfile:
            json.dump(json_comments_in_issue, outfile)
        with open('%s_%s_json_files.json' % (owner,repo_name), 'w') as outfile:
            json.dump(json_files, outfile)
        with open('%s_%s_json_rev_comment.json' % (owner,repo_name), 'w') as outfile:
            json.dump(json_rev_comment, outfile)
        print("finish saving file")
    else:
        with open('%s_%s_pulls_num.json' % (owner,repo_name)) as json_file:
            pulls_num = json.load(json_file)
        with open('%s_%s_json_pulls.json' % (owner,repo_name)) as json_file:
            json_pulls = json.load(json_file)
        with open('%s_%s_json_commit.json' % (owner,repo_name)) as json_file:
            json_commit = json.load(json_file)
        with open('%s_%s_json_comments_in_issue.json' % (owner,repo_name)) as json_file:
            json_comments_in_issue = json.load(json_file)
        with open('%s_%s_json_files.json' % (owner,repo_name)) as json_file:
            json_files = json.load(json_file)
        with open('%s_%s_json_rev_comment.json' % (owner,repo_name)) as json_file:
            json_rev_comment = json.load(json_file)
        print("finish opening file")
    pull_req = PullRequest(json_pulls, json_commit,json_comments_in_issue,json_files,json_rev_comment)
    #
    pulls = {}
    pulls['age'] = pull_req.get_age()
    pulls['con_rate'] = pull_req.get_con_rate()
    pulls['acc_rate'] = pull_req.get_acc_rate()
    pulls['line_add'] = pull_req.get_line_add()
    pulls['line_del'] = pull_req.get_line_del()
    pulls['commit'] = pull_req.get_commit()
    pulls['file'] = pull_req.get_file()
    pulls['comment'] = pull_req.get_comment()
    pulls['rev_comment'] = pull_req.get_rev_comment()
    pulls['core_mem'] = pull_req.get_core_mem()
    pulls['intra_branch'] = pull_req.get_intra_branch()
    pulls['contain_fix'] = pull_req.get_issue_fix()
    pulls['last_comment_mention'] = pull_req.get_last_comment_mention(pulls_num)
    pulls['has_test_code'] = pull_req.get_has_test_code(pulls_num)
    pulls['rev_change'] = pull_req.get_rev_change()
    pulls['churn_rev'] = pull_req.get_churn_rev()
    pulls['self_approve'] = pull_req.get_self_approve()
    pulls['hastily_rev'] = pull_req.get_hastily_rev(pulls['age'])
    pulls['no_discussion'] = pull_req.get_no_discussion(pulls_num)
    pulls['typ_discuss_len'] = pull_req.get_typ_discuss_len(pulls['comment'])
    pulls['typ_rev_window'] = pull_req.get_typ_rev_window(pulls['age'])
    pulls['involve_expert'] = pull_req.get_involve_expert()
    #
    df = pd.DataFrame(pulls)
    df.to_csv(join('..', 'output', 'Samsung_iotjs_modify.csv'))